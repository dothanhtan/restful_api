<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        dd($this);
        return [
            'data' => $this->collection,
            'links'=> [
                'prev' => $this->previousPageUrl(),
                'next' => $this->nextPageUrl()
            ],
            'meta' => [
                'total' => $this->total()
            ]
        ];
    }
}
