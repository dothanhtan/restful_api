<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    protected $post;

    /**
     * @param $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = $this->post->paginate(5);
//        $postResource = PostResource::collection($post)->response()->getData(true);
        $postCollection = new PostCollection($post);
        return $this->sendSuccessResponse($postCollection, 'Success', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $postCreate = $request->all();
        $post = $this->post->create($postCreate);
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, 'Create Successful', Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post->findOrFail($id);
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, 'Success', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $postUpdate = $request->all();
        $post = $this->post->findOrFail($id);
        $post->update($postUpdate);
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, 'Update Successful', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->post->findOrFail($id);
        $post->delete();
        $postResource = new PostResource($post);
        return $this->sendSuccessResponse($postResource, 'Delete Successful', Response::HTTP_OK);
    }
}
